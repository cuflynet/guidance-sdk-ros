#include <motion_sensor.h>

MotionSensor::MotionSensor() : nh("~")
{
  string frame_base("guidance");
  nh.getParam("frame_base", frame_base);
  frameID = frame_base + "_odom";
  pub = nh.advertise<nav_msgs::Odometry>("odom",1);
}

MotionSensor::~MotionSensor()
{
}

void MotionSensor::publish(motion *src)
{
  nav_msgs::Odometry msg;
  msg.header.stamp = ros::Time::now();
  msg.header.frame_id = frameID;

  //Copy from the DJI message to the ROS message
  if (src->position_status)
    {
      msg.pose.pose.position.x = src->position_in_global_x;
      msg.pose.pose.position.y = src->position_in_global_y;
      msg.pose.pose.position.z = src->position_in_global_z;
      
      //Fill the pose covariance diagonals
      msg.pose.covariance[0] = src->uncertainty_location[0];
      msg.pose.covariance[7] = src->uncertainty_location[1];
      msg.pose.covariance[14] = src->uncertainty_location[2];
    }
  
  //No idea what the right order is here for the quat
    if (src->attitude_status)
      {
	msg.pose.pose.orientation.x = src->q0;
	msg.pose.pose.orientation.x = src->q1;
	msg.pose.pose.orientation.x = src->q2;
	msg.pose.pose.orientation.x = src->q3;
      }

    if (src->velocity_status)
      {
	msg.twist.twist.linear.x = src->velocity_in_global_x;
	msg.twist.twist.linear.y = src->velocity_in_global_y;
	msg.twist.twist.linear.z = src->velocity_in_global_z;
	
	//Fill the twist covariance diagonal terms
	msg.twist.covariance[0] = src->uncertainty_velocity[0];
	msg.twist.covariance[7] = src->uncertainty_velocity[1];
	msg.twist.covariance[14] = src->uncertainty_velocity[2];
      }
    pub.publish(msg);
}
