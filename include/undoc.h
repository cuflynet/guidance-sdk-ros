#ifndef __UNDOC_H_
#define __UNDOC_H_
// Undocumented functions in the libDJI_Guidance library
void reboot_fastboot();
void get_current_directory(char* wd); //this is on the host
void enable_adb(bool state);
void enable_mvo(bool state);
void get_soc_version(int& ver);
void get_vision_version(int& ver);
void get_app_id();
void enable_adb(bool);
#endif
