/*
 * GuidanceNode.cpp
 *
 *  Created on: Apr 29, 2015
 */

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <map>
#include <vector>
#include <memory>

#include <iostream>

#include <cv_bridge/cv_bridge.h>


#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "DJI_guidance.h"
#include "DJI_utility.h"
#include <camera_sensors.h>
#include <motion_sensor.h>

#include <camera_info_manager/camera_info_manager.h>
#include <nav_msgs/Odometry.h> //for the new Motion type
#include <geometry_msgs/TransformStamped.h> //IMU
#include <geometry_msgs/Vector3Stamped.h> //velocity
#include <sensor_msgs/LaserScan.h> //obstacle distance & ultrasonic
#include <sensor_msgs/CameraInfo.h> // camera info message. Contains cam params
#include <yaml-cpp/yaml.h> // use to parse YAML calibration file
#include <fstream> // required to parse YAML 

using namespace cv;
ros::Publisher imu_pub;
ros::Publisher obstacle_distance_pub;
ros::Publisher velocity_pub;
ros::Publisher ultrasonic_pub;

shared_ptr<MultiCamSensor> cameras;
shared_ptr<MotionSensor> motion_;

DJI_lock        g_lock;
DJI_event       g_event;
int display = 0;

template<typename T> vector<T> split(const T & str, const T & delimiters);

  /*Split fcn from http://stackoverflow.com/questions/236129/split-a-string-in-c */
template<typename T>
vector<T> split(const T & str, const T & delimiters)
  {
    vector<T> v;
    typename T::size_type start = 0;
    size_t pos = str.find_first_of(delimiters, start);
    while(pos != T::npos) {
      if(pos != start) // ignore empty tokens
	v.push_back(str.substr(start, pos - start));
      start = pos + 1;
      pos = str.find_first_of(delimiters, start);
    }
    if(start < str.length()) // ignore trailing delimiter
      v.push_back(str.substr(start, pos - start)); // add what's left of the string
    return v;
  }


std::ostream& operator<<(std::ostream& out, const e_sdk_err_code value){
  const char* s = 0;
  static char str[100]={0};
#define PROCESS_VAL(p) case(p): s = #p; break;
  switch(value){
    PROCESS_VAL(e_OK);     
    PROCESS_VAL(e_load_libusb_err);     
    PROCESS_VAL(e_sdk_not_inited);
    PROCESS_VAL(e_disparity_not_allowed);
    PROCESS_VAL(e_image_frequency_not_allowed);
    PROCESS_VAL(e_config_not_ready);
    PROCESS_VAL(e_online_flag_not_ready);
    PROCESS_VAL(e_stereo_cali_not_ready);
    PROCESS_VAL(e_libusb_io_err);
    PROCESS_VAL(e_timeout);
  default:
    strcpy(str, "Unknown error");
    s = str;
    break;
  }
#undef PROCESS_VAL

  return out << s;
}


// A nice TODO will be to wrap the driver using the ROS standard cam_info_manager. 
// Hackish code to read cam params from YAML 
// adapted from cam_info_manager and camera_calibration_parser https://github.com/ros-perception/image_common/blob/hydro-devel/camera_calibration_parsers/src/parse_yml.cpp

int my_callback(int data_type, int data_len, char *content)
{
  g_lock.enter();

  /* image data */
  if (e_image == data_type && NULL != content)
    {        
      image_data* data = (image_data*)content;
      cameras->publish(data);
    }
  if (e_motion == data_type && NULL != content)
    {
      //ROS_INFO("Got motion data");
      motion *motion_data = (motion*) content;
      motion_->publish(motion_data);
    }
  /* imu */
  if ( e_imu == data_type && NULL != content )
    {
      imu *imu_data = (imu*)content;

      if (display)
	{
	  printf( "frame index: %d, stamp: %d\n", imu_data->frame_index, imu_data->time_stamp );
	  printf( "imu: [%f %f %f %f %f %f %f]\n", imu_data->acc_x, imu_data->acc_y, imu_data->acc_z, imu_data->q[0], imu_data->q[1], imu_data->q[2], imu_data->q[3] );
 	}
      
      // publish imu data
      geometry_msgs::TransformStamped g_imu;
      g_imu.header.frame_id = "guidance";
      g_imu.header.stamp    = ros::Time::now();
      g_imu.transform.translation.x = imu_data->acc_x;
      g_imu.transform.translation.y = imu_data->acc_y;
      g_imu.transform.translation.z = imu_data->acc_z;
      g_imu.transform.rotation.w = imu_data->q[0];
      g_imu.transform.rotation.z = imu_data->q[1];
      g_imu.transform.rotation.y = imu_data->q[2];
      g_imu.transform.rotation.z = imu_data->q[3];
      imu_pub.publish(g_imu);
    }
  /* velocity */
  if ( e_velocity == data_type && NULL != content )
    {
      velocity *vo = (velocity*)content;
      if (display)
	{
	  printf( "frame index: %d, stamp: %d\n", vo->frame_index, vo->time_stamp );
	  printf( "vx:%f vy:%f vz:%f\n", 0.001f * vo->vx, 0.001f * vo->vy, 0.001f * vo->vz );
	}
      // publish velocity
      geometry_msgs::Vector3Stamped g_vo;
      g_vo.header.frame_id = "guidance";
      g_vo.header.stamp    = ros::Time::now();
      g_vo.vector.x = 0.001f * vo->vx;
      g_vo.vector.y = 0.001f * vo->vy;
      g_vo.vector.z = 0.001f * vo->vz;
      velocity_pub.publish(g_vo);
    }

  /* obstacle distance */
  if ( e_obstacle_distance == data_type && NULL != content )
    {
      obstacle_distance *oa = (obstacle_distance*)content;

      if (display)
	{
	  printf( "frame index: %d, stamp: %d\n", oa->frame_index, oa->time_stamp );
	  printf( "obstacle distance:" );
	  for ( int i = 0; i < CAMERA_PAIR_NUM; ++i )
	    {
	      printf( " %f ", 0.01f * oa->distance[i] );
	    }
	  printf( "\n" );
	}
      
      // publish obstacle distance
      sensor_msgs::LaserScan g_oa;
      g_oa.ranges.resize(CAMERA_PAIR_NUM);
      g_oa.header.frame_id = "guidance";
      g_oa.header.stamp    = ros::Time::now();
      for ( int i = 0; i < CAMERA_PAIR_NUM; ++i )
	g_oa.ranges[i] = 0.01f * oa->distance[i];
      obstacle_distance_pub.publish(g_oa);
    }

  /* ultrasonic */
  if ( e_ultrasonic == data_type && NULL != content )
    {
      ultrasonic_data *ultrasonic = (ultrasonic_data*)content;

      if (display)
	{
	  
	  printf( "frame index: %d, stamp: %d\n", ultrasonic->frame_index, ultrasonic->time_stamp );
	  for ( int d = 0; d < CAMERA_PAIR_NUM; ++d )
	    {
	      printf( "ultrasonic distance: %f, reliability: %d\n", ultrasonic->ultrasonic[d] * 0.001f, (int)ultrasonic->reliability[d] );
	    }
	}
      
      // publish ultrasonic data
      sensor_msgs::LaserScan g_ul;
      g_ul.ranges.resize(CAMERA_PAIR_NUM);
      g_ul.intensities.resize(CAMERA_PAIR_NUM);
      g_ul.header.frame_id = "guidance";
      g_ul.header.stamp    = ros::Time::now();
      for ( int d = 0; d < CAMERA_PAIR_NUM; ++d ){
	g_ul.ranges[d] = 0.001f * ultrasonic->ultrasonic[d];
	g_ul.intensities[d] = 1.0 * ultrasonic->reliability[d];
      }
      ultrasonic_pub.publish(g_ul);
    }

  g_lock.leave();
  g_event.set_event();

  return 0;
}

#define RETURN_IF_ERR(err_code) { if( err_code ){ release_transfer();	\
						  std::cout<<"Error: "<<(e_sdk_err_code)err_code<<" at "<<__LINE__<<","<<__FILE__<<std::endl; return -1;}}

void sig_handler(int signo)
{
  if (signo == SIGINT)
    {
      printf("Guidance: received SIGINT\n");
      /* release data transfer */
      stop_transfer();
      
      //make sure the ack packet from GUIDANCE is received
      sleep(1);
      
      release_transfer();
      exit(0);
    }
}



int main(int argc, char** argv)
{

  /* initialize ros */
  ros::init(argc, argv, "GuidanceNode",ros::init_options::NoSigintHandler);
  ros::NodeHandle my_node("~");

  imu_pub  				= my_node.advertise<geometry_msgs::TransformStamped>("imu",1);
  velocity_pub  			= my_node.advertise<geometry_msgs::Vector3Stamped>("velocity",1);
  obstacle_distance_pub	= my_node.advertise<sensor_msgs::LaserScan>("obstacle_distance",1);
  ultrasonic_pub			= my_node.advertise<sensor_msgs::LaserScan>("ultrasonic",1);

  vector<string> cameraStrList;
  vector<uint8_t> cameraIntList;
  
  string cameraString;
 
  my_node.getParam("display", display);
  my_node.getParam("cameras", cameraString);

  cameraStrList = split<string>(cameraString,",");

  /* Install our custom SIGINT handler to shutdown Guidance cleanly */
  signal(SIGINT, sig_handler);
  
  /* initialize guidance */
  reset_config();
  int err_code = init_transfer();
  RETURN_IF_ERR(err_code);

  int online_status[CAMERA_PAIR_NUM];
  err_code = get_online_status(online_status);
  RETURN_IF_ERR(err_code);
  std::cout<<"Sensor online status: ";
  for (int i=0; i<CAMERA_PAIR_NUM; i++)
    std::cout<<online_status[i]<<" ";
  std::cout<<std::endl;

  // get cali param
  stereo_cali cali[CAMERA_PAIR_NUM];
  err_code = get_stereo_cali(cali);
  RETURN_IF_ERR(err_code);
  std::cout<<"cu\tcv\tfocal\tbaseline\n";
  for (int i=0; i<CAMERA_PAIR_NUM; i++)
    {
      std::cout<<cali[i].cu<<"\t"<<cali[i].cv<<"\t"<<cali[i].focal<<"\t"<<cali[i].baseline<<std::endl;
    }
	
  /* select data */
  vector<camera_index_t> leftCams;
  vector<camera_index_t> rightCams;
  vector<camera_index_t> depthCams;
  for (int i = 0; i< 5; i++)
    {
      //Pick up all of the cameras
      //Better: Parse the camera string list to select a subset
      /* second parameter: left/right camera */
  
        e_vbus_index camID = (e_vbus_index) (i);
       err_code = select_greyscale_image(camID, true);
       RETURN_IF_ERR(err_code);
       err_code = select_greyscale_image(camID, false);
       RETURN_IF_ERR(err_code);
       //err_code = select_depth_image(camID);
       //RETURN_IF_ERR(err_code);
       
       leftCams.push_back(i);
       rightCams.push_back(i);
       //depthCams.push_back(i);
    }


  cameras = make_shared<MultiCamSensor>(leftCams, rightCams, depthCams);
  motion_ = make_shared<MotionSensor>();
  
  //  err_code = select_depth_image(camID_);
  err_code = set_image_frequecy(e_frequecy_20);
  RETURN_IF_ERR(err_code);
  select_motion();
  select_imu();
  select_ultrasonic();
  select_obstacle_distance();
  select_velocity();
  /* start data transfer */
  err_code = set_sdk_event_handler(my_callback);
  RETURN_IF_ERR(err_code);
  err_code = start_transfer();
  RETURN_IF_ERR(err_code);
	
  ros::spin();

  /* release data transfer */
  err_code = stop_transfer();
  RETURN_IF_ERR(err_code);
  //make sure the ack packet from GUIDANCE is received
  sleep(1);

  err_code = release_transfer();
  RETURN_IF_ERR(err_code);

  return 0;
}


    
