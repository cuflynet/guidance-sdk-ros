#ifndef _MOTION_SENSOR_H
#define _MOTION_SENSOR_H

#include <ros/ros.h>
#include <DJI_guidance.h>
#include <nav_msgs/Odometry.h>

using namespace std;

class MotionSensor
{
public:
  MotionSensor();
  ~MotionSensor();
  void publish(motion *msg);
private:
  ros::Publisher pub;
  ros::NodeHandle nh;
  string frameID;
};
  
#endif
