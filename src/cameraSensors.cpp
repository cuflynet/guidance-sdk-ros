#include <camera_sensors.h>

//Use CameraInfoManager to handle cal params, etc

CameraSensor::CameraSensor(ros::NodeHandle _parent, string _ns, camera_index_t _index, uint8_t _bpp) :
  ns(_ns), index(_index),
  bpp(_bpp)
{
  
  nh = ros::NodeHandle(_parent, _ns + "/cam" + to_string(_index));
  cinfo_ = make_shared<camera_info_manager::CameraInfoManager>(nh);
  it_ = make_shared<image_transport::ImageTransport>(nh);
  
  string topic = "image_raw";
  imagePub = it_->advertiseCamera(topic,1);
  cameraName = "guidance_" + _ns + "_cam" + to_string(_index);
  if (!cinfo_->setCameraName(cameraName))
    {
      ROS_WARN_STREAM("[" << cameraName << "] not valid for camera_info_manager");
    }
  string frame_base("guidance");
  nh.getParam("frame_base", frame_base);
  
  frameID = frame_base + "_" + _ns + "_cam" + to_string(_index) + "_optical_frame"; 
}

CameraSensor::~CameraSensor()
{}

void CameraSensor::publish(unsigned int frame_index, unsigned int time_stamp, char* buffer)
{
  
  // get current CameraInfo data
  sensor_msgs::CameraInfo
    ci(cinfo_->getCameraInfo());

  sensor_msgs::Image msg;
  msg.header.stamp = ros::Time::now();
  msg.header.frame_id = frameID; //coordinate frame name

  ci.header.stamp = msg.header.stamp;
  ci.header.frame_id = msg.header.frame_id;
  
  string encoding;
  if (bpp == 1)
    {
      encoding = sensor_msgs::image_encodings::MONO8;
    }
  else
    {
      encoding = sensor_msgs::image_encodings::MONO16;
    }
  ci.height = IMAGE_HEIGHT;
  ci.width = IMAGE_WIDTH;
  sensor_msgs::fillImage(msg, encoding, ci.height, ci.width, ci.width*bpp, buffer);
  imagePub.publish(msg, ci);
  
}

MultiCamSensor::MultiCamSensor(vector<camera_index_t> _leftIndices,
			       vector<camera_index_t> _rightIndices,
			       vector<camera_index_t> _depthIndices
			       )
{
  ros::NodeHandle parent("~");
  for (camera_index_t index : _leftIndices)
    {
      string ns("left");
      shared_ptr<CameraSensor> newCam = make_shared<CameraSensor>(parent, ns, index, 1);
      leftSensors.insert(pair<camera_index_t, shared_ptr<CameraSensor>> (index, newCam));
    }
  
  for (camera_index_t index : _rightIndices)
    {
      string ns("right");
      shared_ptr<CameraSensor> newCam = make_shared<CameraSensor>(parent, ns, index, 1);
      rightSensors.insert(pair<camera_index_t, shared_ptr<CameraSensor>> (index, newCam));
    }
  for (camera_index_t index : _depthIndices)
    {
      string ns("depth");
      shared_ptr<CameraSensor> newCam = make_shared<CameraSensor>(parent, ns, index, 2);
      depthSensors.insert(pair<camera_index_t, shared_ptr<CameraSensor>> (index, newCam));
    }
}

MultiCamSensor::~MultiCamSensor()
{
}

void MultiCamSensor::publish(image_data* data)
{
  //Given a message that is of type image, figure out which one of our children gets it
  //e_vbus_index ranges from 0-4 (ports 1-5)

  //Ask the map for the ith CameraSensor object, potentially non-contiguous
  
  for (uint8_t i = 0; i<5; i++)
    {
      if (data->m_greyscale_image_left[i])
	{
	  leftSensors.at(i)->publish(data->frame_index, data->time_stamp, data->m_greyscale_image_left[i]);
	}
      if (data->m_greyscale_image_right[i])
	{
	  rightSensors.at(i)->publish(data->frame_index, data->time_stamp, data->m_greyscale_image_right[i]);
	}
      if (data->m_depth_image[i])
	{
	  depthSensors.at(i)->publish(data->frame_index, data->time_stamp, data->m_depth_image[i]);
	}
    }
}
