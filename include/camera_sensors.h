#ifndef _CAMERA_SENSORS_H_
#define _CAMERA_SENSORS_H_

#include <map>
#include <vector>
#include <memory>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/fill_image.h>
#include <camera_info_manager/camera_info_manager.h>
#include "DJI_guidance.h"
#include "DJI_utility.h"



//Common class for every camera sensor
//Given the index, load params and publish data
//CameraInfoManager code pulled from camera1394 as inspiration per the cim wiki

//Index is of the form [0-9][l,r] as a length 2 C-style string
#define IMAGE_WIDTH 320
#define IMAGE_HEIGHT 240
#define IMAGE_SIZE (IMAGE_HEIGHT * IMAGE_WIDTH)

using namespace std;

/* typedef struct CameraIndex */
/* { */
/*   uint8_t index; */
/*   CameraIndex(uint8_t _index) : */
/*     index(_index) {}; */
/* } camera_index_t; */

typedef uint8_t camera_index_t;

class CameraSensor
{
public:
  CameraSensor(ros::NodeHandle _parent, string ns, camera_index_t index, uint8_t _bpp);
  ~CameraSensor();
  void publish(unsigned int frame_index, unsigned int time_stamp, char* message); 
private:
  shared_ptr<camera_info_manager::CameraInfoManager> cinfo_;
  ros::NodeHandle nh;
  string ns;
  string cameraName;
  string frameID;
  uint8_t bpp;
  camera_index_t index;
  shared_ptr<image_transport::ImageTransport> it_;
  image_transport::CameraPublisher imagePub;
};

class MultiCamSensor
{
public:
  MultiCamSensor(vector<camera_index_t> leftIndices,
		 vector<camera_index_t> rightIndices,
		 vector<camera_index_t> depthIndices);
  ~MultiCamSensor();
  void publish(image_data* data);
  
private:
  map<camera_index_t, shared_ptr<CameraSensor>> leftSensors;
  map<camera_index_t, shared_ptr<CameraSensor>> rightSensors;
  map<camera_index_t, shared_ptr<CameraSensor>> depthSensors;
};

#endif
