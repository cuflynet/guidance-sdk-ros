/*
 * GuidanceNode.cpp
 *
 *  Created on: Apr 29, 2015
 */

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "DJI_guidance.h"
#include "DJI_utility.h"

#include <geometry_msgs/TransformStamped.h> //IMU
#include <geometry_msgs/Vector3Stamped.h> //velocity
#include <sensor_msgs/LaserScan.h> //obstacle distance & ultrasonic

ros::Publisher depth_image_pub;
ros::Publisher left_image_pub;
ros::Publisher right_image_pub;
ros::Publisher imu_pub;
ros::Publisher obstacle_distance_pub;
ros::Publisher velocity_pub;
ros::Publisher ultrasonic_pub;
ros::Publisher position_pub;

using namespace cv;

int WIDTH=320;
int HEIGHT=240;
#define IMAGE_SIZE (HEIGHT * WIDTH)

e_vbus_index camID_;
DJI_lock        g_lock;
DJI_event       g_event;
Mat             g_greyscale_image_left(HEIGHT, WIDTH, CV_8UC1);
Mat				g_greyscale_image_right(HEIGHT, WIDTH, CV_8UC1);
Mat				g_depth(HEIGHT,WIDTH,CV_16SC1);

std::ostream& operator<<(std::ostream& out, const e_sdk_err_code value){
	const char* s = 0;
	static char str[100]={0};
#define PROCESS_VAL(p) case(p): s = #p; break;
	switch(value){
		PROCESS_VAL(e_OK);     
		PROCESS_VAL(e_load_libusb_err);     
		PROCESS_VAL(e_sdk_not_inited);
		PROCESS_VAL(e_disparity_not_allowed);
		PROCESS_VAL(e_image_frequency_not_allowed);
		PROCESS_VAL(e_config_not_ready);
		PROCESS_VAL(e_online_flag_not_ready);
		PROCESS_VAL(e_stereo_cali_not_ready);
		PROCESS_VAL(e_libusb_io_err);
		PROCESS_VAL(e_timeout);
	default:
		strcpy(str, "Unknown error");
		s = str;
		break;
	}
#undef PROCESS_VAL

	return out << s;
}

void writePGM_16(uint16_t *src, int serial, int width, int height)
{
  char fileName[32];
  sprintf(fileName, "image%05d.pgm", serial);
  FILE* outFile = fopen(fileName, "w");
  fprintf(outFile, "P2\n%d %d\n255\n", width, height);
  //fprintf(outFile, "# %s\n", comment);

  //make an image time comment out of the header
  //double imageTime = src->header.stamp.sec + ((double)src->header.stamp.nsec/1e9);
  //fprintf(outFile, "# %1.9f\n", imageTime);

  
  int i,j;


  uint16_t *srcBase = src;
  uint16_t trimVal;
  for (i =0; i<height; i++)
    {
      for (j=0; j<width; j++)
	{
	  trimVal = ((float)(srcBase[i*width + j]) / 2048.0 * 400.0);
	  if (trimVal > 60)
	    trimVal = 250;
	  else
	    trimVal = 10;
	  fprintf(outFile, "%u ", trimVal );
	}
      fprintf(outFile, "\n");
    }

  fclose(outFile);
}

int my_callback(int data_type, int data_len, char *content)
{
    g_lock.enter();

    /* image data */
    if (e_image == data_type && NULL != content)
    {        
        image_data* data = (image_data*)content;

		if ( data->m_greyscale_image_left[camID_] ){
			memcpy(g_greyscale_image_left.data, data->m_greyscale_image_left[camID_], IMAGE_SIZE);
			//imshow("left",  g_greyscale_image_left);
			// publish left greyscale image
			cv_bridge::CvImage left_8;
			g_greyscale_image_left.copyTo(left_8.image);
			left_8.header.frame_id  = "guidance";
			left_8.header.stamp	= ros::Time::now();
			left_8.encoding		= sensor_msgs::image_encodings::MONO8;
			left_image_pub.publish(left_8.toImageMsg());
		}
		if ( data->m_greyscale_image_right[camID_] ){
			memcpy(g_greyscale_image_right.data, data->m_greyscale_image_right[camID_], IMAGE_SIZE);
			//imshow("right", g_greyscale_image_right);
			// publish right greyscale image
			cv_bridge::CvImage right_8;
			g_greyscale_image_right.copyTo(right_8.image);
			right_8.header.frame_id  = "guidance";
			right_8.header.stamp	 = ros::Time::now();
			right_8.encoding  	 = sensor_msgs::image_encodings::MONO8;
			right_image_pub.publish(right_8.toImageMsg());
		}
		if ( data->m_depth_image[camID_] ){
		  /*
		  int i;
		  uint16_t *pixel;
		  pixel = (uint16_t*) data->m_depth_image[camID_];
		  printf("FrameStart:\n");
		  for (i=60; i<WIDTH; i++)
		    {
		      printf("0x%u ",pixel[WIDTH*2*50 + i]);
		    }
		  printf("\n");
		  
		  
		  writePGM_16(pixel, 1, WIDTH, HEIGHT);
		  */
		  memcpy(g_depth.data, data->m_depth_image[camID_]+70, IMAGE_SIZE * sizeof(uint16_t));
			Mat depthf(HEIGHT,WIDTH,CV_32FC1);
			g_depth.convertTo(depthf, CV_32FC1, 1.0/2048.0);
			//imshow("depth", depthf);
			//publish depth image
			//there's probably a better way to do this than two copies..
			//The ros std depth image is a 32FC1, not a 16UC1...
			//publish as a depth_raw
			cv_bridge::CvImage depth_16;
			g_depth.copyTo(depth_16.image);
			depth_16.header.frame_id  = "guidance";
			depth_16.header.stamp	  = ros::Time::now();
			depth_16.encoding	  = sensor_msgs::image_encodings::MONO16;
			depth_image_pub.publish(depth_16.toImageMsg());
		}
		//waitKey(1);
    }

    /* imu */
    if ( e_imu == data_type && NULL != content )
    {
        imu *imu_data = (imu*)content;
        //printf( "frame index: %d, stamp: %d\n", imu_data->frame_index, imu_data->time_stamp );
	// printf( "imu: [%f %f %f %f %f %f %f]\n", imu_data->acc_x, imu_data->acc_y, imu_data->acc_z, imu_data->q[0], imu_data->q[1], imu_data->q[2], imu_data->q[3] );
 	
    	// publish imu data
		geometry_msgs::TransformStamped g_imu;
		g_imu.header.frame_id = "guidance";
		g_imu.header.stamp    = ros::Time::now();
		g_imu.transform.translation.x = imu_data->acc_x;
		g_imu.transform.translation.y = imu_data->acc_y;
		g_imu.transform.translation.z = imu_data->acc_z;
		g_imu.transform.rotation.w = imu_data->q[0];
		g_imu.transform.rotation.x = imu_data->q[1];
		g_imu.transform.rotation.y = imu_data->q[2];
		g_imu.transform.rotation.z = imu_data->q[3];
		imu_pub.publish(g_imu);
    }
    /* velocity */
    if ( e_velocity == data_type && NULL != content )
    {
        velocity *vo = (velocity*)content;
	/*
        printf( "frame index: %d, stamp: %d\n", vo->frame_index, vo->time_stamp );
        printf( "vx:%f vy:%f vz:%f\n", 0.001f * vo->vx, 0.001f * vo->vy, 0.001f * vo->vz );
	*/
		// publish velocity
		geometry_msgs::Vector3Stamped g_vo;
		g_vo.header.frame_id = "guidance";
		g_vo.header.stamp    = ros::Time::now();
		g_vo.vector.x = 0.001f * vo->vx;
		g_vo.vector.y = 0.001f * vo->vy;
		g_vo.vector.z = 0.001f * vo->vz;
		velocity_pub.publish(g_vo);
    }

    /* obstacle distance */
    if ( e_obstacle_distance == data_type && NULL != content )
    {
        obstacle_distance *oa = (obstacle_distance*)content;
	
	/*
        printf( "frame index: %d, stamp: %d\n", oa->frame_index, oa->time_stamp );
        printf( "obstacle distance:" );
        for ( int i = 0; i < CAMERA_PAIR_NUM; ++i )
        {
            printf( " %f ", 0.01f * oa->distance[i] );
        }
		printf( "\n" );
	*/
	
		// publish obstacle distance
		sensor_msgs::LaserScan g_oa;
		g_oa.ranges.resize(CAMERA_PAIR_NUM);
		g_oa.header.frame_id = "guidance";
		g_oa.header.stamp    = ros::Time::now();
		for ( int i = 0; i < CAMERA_PAIR_NUM; ++i )
			g_oa.ranges[i] = 0.01f * oa->distance[i];
		obstacle_distance_pub.publish(g_oa);
	}

    /* ultrasonic */
    if ( e_ultrasonic == data_type && NULL != content )
    {
        ultrasonic_data *ultrasonic = (ultrasonic_data*)content;
	/*
        printf( "frame index: %d, stamp: %d\n", ultrasonic->frame_index, ultrasonic->time_stamp );
        for ( int d = 0; d < CAMERA_PAIR_NUM; ++d )
        {
            printf( "ultrasonic distance: %f, reliability: %d\n", ultrasonic->ultrasonic[d] * 0.001f, (int)ultrasonic->reliability[d] );
        }
	*/
	
	
		// publish ultrasonic data
		sensor_msgs::LaserScan g_ul;
		g_ul.ranges.resize(CAMERA_PAIR_NUM);
		g_ul.intensities.resize(CAMERA_PAIR_NUM);
		g_ul.header.frame_id = "guidance";
		g_ul.header.stamp    = ros::Time::now();
		for ( int d = 0; d < CAMERA_PAIR_NUM; ++d ){
			g_ul.ranges[d] = 0.001f * ultrasonic->ultrasonic[d];
			g_ul.intensities[d] = 1.0 * ultrasonic->reliability[d];
		}
		ultrasonic_pub.publish(g_ul);
    }
	
	if(e_motion == data_type && NULL!=content){
		motion* m=(motion*)content;
		printf("frame index: %d, stamp: %d\n", m->frame_index, m->time_stamp);
		printf("(px,py,pz)=(%.2f,%.2f,%.2f)\n", m->position_in_global_x, m->position_in_global_y, m->position_in_global_z);

		// publish position
		geometry_msgs::Vector3Stamped g_pos;
		g_pos.header.frame_id = "guidance";
		g_pos.header.stamp = ros::Time::now();
		g_pos.vector.x = m->position_in_global_x;
		g_pos.vector.y = m->position_in_global_y;
		g_pos.vector.z = m->position_in_global_z;
		position_pub.publish(g_pos);
	}
	
    g_lock.leave();
    g_event.set_event();

    return 0;
}

#define RETURN_IF_ERR(err_code) { if( err_code ){ release_transfer(); \
std::cout<<"Error: "<<(e_sdk_err_code)err_code<<" at "<<__LINE__<<","<<__FILE__<<std::endl; return -1;}}

int main(int argc, char** argv)
{

  /* initialize ros */
  ros::init(argc, argv, "GuidanceNode");
  ros::NodeHandle my_node("~");
  depth_image_pub			= my_node.advertise<sensor_msgs::Image>("/guidance/depth_image_raw",1);
  left_image_pub			= my_node.advertise<sensor_msgs::Image>("/guidance/left_image",1);
  right_image_pub			= my_node.advertise<sensor_msgs::Image>("/guidance/right_image",1);
  imu_pub  				= my_node.advertise<geometry_msgs::TransformStamped>("/guidance/imu",1);
  velocity_pub  			= my_node.advertise<geometry_msgs::Vector3Stamped>("/guidance/velocity",1);
  obstacle_distance_pub	= my_node.advertise<sensor_msgs::LaserScan>("/guidance/obstacle_distance",1);
  ultrasonic_pub			= my_node.advertise<sensor_msgs::LaserScan>("/guidance/ultrasonic",1);

  int32_t tempCam;
  my_node.param<int32_t>("camera_id", tempCam, (int32_t)e_vbus1);
  camID_ = (e_vbus_index) tempCam;
  ROS_INFO("Guidance: Using camera ID: %u", (uint32_t)camID_);

  /* initialize guidance */
  reset_config();
  int err_code = init_transfer();
  RETURN_IF_ERR(err_code);

  int online_status[CAMERA_PAIR_NUM];
  err_code = get_online_status(online_status);
  RETURN_IF_ERR(err_code);
  std::cout<<"Sensor online status: ";
  for (int i=0; i<CAMERA_PAIR_NUM; i++)
    std::cout<<online_status[i]<<" ";
  std::cout<<std::endl;

  // get cali param
  stereo_cali cali[CAMERA_PAIR_NUM];
  err_code = get_stereo_cali(cali);
  RETURN_IF_ERR(err_code);
  std::cout<<"cu\tcv\tfocal\tbaseline\n";
  for (int i=0; i<CAMERA_PAIR_NUM; i++)
    {
      std::cout<<cali[i].cu<<"\t"<<cali[i].cv<<"\t"<<cali[i].focal<<"\t"<<cali[i].baseline<<std::endl;
    }

	
  /* select data */
    
  err_code = select_greyscale_image(camID_, true);
  RETURN_IF_ERR(err_code);
  err_code = select_greyscale_image(camID_, false);
  RETURN_IF_ERR(err_code);
  err_code = select_depth_image(camID_);
  RETURN_IF_ERR(err_code);
  err_code = set_image_frequecy(e_frequecy_20);
  RETURN_IF_ERR(err_code);
  select_imu();
  select_ultrasonic();
  select_obstacle_distance();
  select_velocity();

  /* start data transfer */
  err_code = set_sdk_event_handler(my_callback);
  RETURN_IF_ERR(err_code);
  err_code = start_transfer();
  RETURN_IF_ERR(err_code);
	
  // for setting exposure
  exposure_param para;
  para.m_is_auto_exposure = 1;
  para.m_step = 10;
  para.m_expected_brightness = 120;
  para.m_camera_pair_index = camID_;

  ros::spin();

  /* release data transfer */
  err_code = stop_transfer();
  RETURN_IF_ERR(err_code);
  //make sure the ack packet from GUIDANCE is received
  sleep(1);
  
  err_code = release_transfer();
  RETURN_IF_ERR(err_code);
  
  return 0;
}
