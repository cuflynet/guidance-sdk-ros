#include <stdio.h>
#include <string.h>
#include <iostream>

#include "DJI_guidance.h"
#include "DJI_utility.h"
#include "undoc.h"
int main(int argc, char** argv)
{

  /* initialize guidance */
  reset_config();
  int err_code = init_transfer();
 
  int ver;
  enable_adb(true);
  
  /* release data transfer */
  err_code = stop_transfer();

  //make sure the ack packet from GUIDANCE is received
  sleep(1);
  
  err_code = release_transfer();

  
  return 0;
}
